# slideshell

Apresentação de slides na linha de comando

## Descrição

O script 'slideshell' lê todos os arquivos no caminho informado e monta uma apresentação. Os slides são carregados conforme a ordem alfanumérica de seus nomes. Caso a pasta de slides esteja vazia, um novo slide será criado automaticamente com o nome 'slide-01'.

É possível informar uma mensagem (entre aspas) para ser exibida no rodapé da apresentação. Caso não seja fornecida uma mensagem, será exibido o caractere `:` em seu lugar.

## Instalação

Clone o repositório, entre na pasta `slideshell` e execute o script de instalação:

```
:~$ git clone https://gitlab.com/blau_araujo/slideshell.git
:~$ cd slideshell
:~/slideshell$ ./install.sh
```

Para desisntalar, entre na pasta clonada e execute:

```
:~/slideshell$ ./uninstall.sh
```

Para atualizar, entre na pasta clonada e execute:

```
:~/slideshell$ ./update.sh
```

## Uso

```
slideshell CAMINHO_DOS_SLIDES ['MENSAGEM DE RODAPÉ']
```

## Comandos

| Teclas               | Ação                    |
|----------------------|-------------------------|
| Seta para cima       | Primeiro slide          |
| Seta para baixo      | Último slide            |
| Seta para a direita  | Próximo slide           |
| Seta para a esquerda | Slide anterior          |
| `G` ou `g`           | Ir para o slide 'n'     |
| `A` ou `a`           | Adicionar novo slide    |
| `E` ou `e`           | Editar slide            |
| `K` ou `k`           | Matar slide             |
| `U` ou `u`           | Restaurar slide         |
| `R` ou `r`           | Recarregar apresentação |
| `Q` ou `q`           | Sair                    |

# Formatação dos slides

Utilizar os caracteres abaixo no ínício da linha seguidos de um espaço aplica as seguintes formatações:

| Caractere | Formato     | Estilos aplicados          |
|------|------------------|----------------------------|
| `#`  | Título 1         | azul, negrito, esquerda    |
| `##` | Título 2         | verelho, negrito, recuado  |
| `+`  | Título 3         | verde, negrito, recuado    |
| `:`  | Código/notas     | amarelo, recuado           |
| `*`  | Lista            | ciano, recuado, asterisco  |
| `-`  | Subitem de lista | ciano, duplo recuo, traço  |

* Números seguidos de espaço traço e espaço geram listas numeradas (manualmente) em cinza claro.

* Sem os caracteres de formatação no ínício de linha, o texto será exibido recuado e em cinza claro.

## Configuração

O arquivo da configuração padrão fica em `/etc/slideshellrc` e pode ser copiado para a pasta `home` do usuário para ser modificado conforme suas preferências:

```
:~$ cp /etc/slideshellrc $HOME/.slideshellrc
```

Alternativamente, você pode copiar o arquivo do repositório clonado para `/home/seu_nome_de_usuario` com o nome de `.slideshellrc`.
